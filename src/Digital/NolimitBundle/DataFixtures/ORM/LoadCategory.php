<?php
namespace Digital\NolimitBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Digital\NolimitBundle\Entity\Categorie;



class LoadCategory
{
	public function load(ObjectManager $manager)
	{

		$categories =["Policier", "Romance", "Comédie", "Action"];
		//foreach( $categories as $name ) :
			$category = new Categorie();
			$category->setName("Action");
			$manager->persist($category);
			$this->addReference("Action", $category);

		//endforeach;
		$manager->flush();

	}

	public function getOrder()
	{
		// the order in which fixtures will be loaded
		// the lower the number, the sooner that this fixture is loaded
		return 1;
	}


}