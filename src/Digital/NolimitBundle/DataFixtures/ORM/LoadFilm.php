<?php
namespace Digital\NolimitBundle\DataFixtures\ORM;

use Digital\NolimitBundle\Entity\Photo;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Digital\NolimitBundle\Entity\Film;
use Digital\NolimitBundle\Entity\Categorie;


class LoadFilm extends AbstractFixture implements OrderedFixtureInterface
{
	private static  $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam condimentum non eros vitae scelerisque. Proin blandit faucibus magna id ullamcorper. Cras a risus imperdiet, lobortis sem nec, venenatis mi. Nam elementum velit convallis commodo laoreet. Pellentesque malesuada magna diam, in vulputate lacus hendrerit in. Phasellus quis mi scelerisque, molestie enim aliquet, aliquet sem. Etiam pulvinar luctus urna vitae gravida.

Etiam sodales congue est sed faucibus. Donec ut purus et nisl efficitur varius nec dapibus arcu. Nunc placerat felis quis ligula rutrum, sit amet iaculis nulla tristique. In vitae nulla vel eros ornare tincidunt eget at nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu purus eu felis consectetur porttitor. Nam pellentesque, mi et convallis ultrices, turpis enim laoreet ex, at tristique risus arcu vitae diam. In nec nisl varius, rutrum mi eu, tristique nisi. Nulla malesuada id lacus eget pulvinar. Fusce lobortis interdum iaculis. Sed semper suscipit arcu, volutpat auctor ante ullamcorper non. Pellentesque placerat rhoncus quam, mollis fringilla felis mollis ut. Vestibulum placerat finibus odio, sed auctor nunc malesuada nec. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec viverra condimentum est, ut molestie tellus mollis id.";


	public function load(ObjectManager $manager) {


		$category = new Categorie();
		$category->setName("Action");


		$category2 = new Categorie();
		$category2->setName("Policier");

		$film = new Film();
		$film->setTitre( "Piège de cristal" );
			$film->setDescription( self::$description );
		$file = new Photo();
		$file->setAlt("no photo");

				$film2 = new Film();

		$film->setCategorie($category);

	//film2
		$film2->setTitre( "L'Arme fatale" );
			$film2->setDescription( self::$description );
		$file->setAlt("no photo");
		$film2->setCategorie($category2);




		$manager->persist($category);
		$manager->persist( $film );;
		$manager->persist( $film2 );

		$manager->flush();
	}

	public function getOrder()
	{
		// the order in which fixtures will be loaded
		// the lower the number, the sooner that this fixture is loaded
		return 2;
	}

	
}