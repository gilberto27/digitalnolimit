<?php
namespace Digital\NolimitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Digital\NolimitBundle\Entity\Photo;
use Digital\NolimitBundle\Entity\Categorie;

/**
 * Film
 *
 * @ORM\Table(name="film")
 * @ORM\Entity(repositoryClass="Digital\NolimitBundle\Repository\FilmRepository")
 */
class Film
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 10,
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string")
     */
    private $titre;



    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=2500)
     */
    private $description;

    /**
     * @var Photo
     * @ORM\OneToOne(targetEntity="\Digital\NolimitBundle\Entity\Photo", cascade={"persist"})
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="\Digital\NolimitBundle\Entity\Categorie", inversedBy="films", cascade={"persist"})
     */
    private $categorie;

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set $titre
     *
     * @param string $titre
     *
     * @return Film
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Film
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set categorie
     *
     * @param  \Digital\NolimitBundle\Entity\Categorie $categorie
     *
     * @return Film
     */
    public function setCategorie(\Digital\NolimitBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }


    /**
     * Get \Digital\NolimitBundle\Entity\Categorie
     *
     * @return Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set file
     *
     * @param \Digital\NolimitBundle\Entity\Photo
     *
     * @return Film
     */
    public function setFile(\Digital\NolimitBundle\Entity\Photo $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \Digital\NolimitBundle\Entity\Photo
     */
    public function getFile()
    {
        return $this->file;
    }

}

