<?php
namespace Digital\NolimitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Digital\NolimitBundle\Entity\Film;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Photo
 * @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="Digital\NolimitBundle\Repository\PhotoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @Assert\File(
     *     maxSize = "3M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
      )
     * @ORM\Column(name="url", type="string", length=155, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     */
    private $alt;

    private $file;

    // On ajoute cet attribut pour y stocker le nom du fichier temporairement
    private $tempFilename;


    private $path;

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param UploadedFile $file
     * @return Image
     * @internal param string $url
     *
     */
    public function setUrl( $file = null)
    {
        $this->url = $file;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return Image
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }



    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */

    public function setFile( UploadedFile $file)
    {

        $this->file = $file;

    }
    
    public function getUploadPath() {
        
        return "uploads/".$this->url;
    }
}

