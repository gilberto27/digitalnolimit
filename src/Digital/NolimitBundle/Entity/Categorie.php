<?php
namespace Digital\NolimitBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

use Digital\NolimitBundle\Entity\Film;


/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 *@ORM\Entity(repositoryClass="Digital\NolimitBundle\Repository\CategorieRepository")
 */
class Categorie
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity="Film", mappedBy="categorie")
	 */
	private $films;



	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Category
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->films = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add film
	 *
	 * @param \Digital\NolimitBundle\Entity\Film $film
	 *
	 * @return Categorie
	 */
	public function addFilm(\Digital\NolimitBundle\Entity\Film $film)
	{
		$this->films[] = $film;

		return $this;
	}

	/**
	 * Remove film
	 *
	 * @param \Digital\NolimitBundle\Entity\Film $fil
	 */
	public function removeFilm(\Digital\NolimitBundle\Entity\Film $film)
	{
		$this->films->removeElement($film);
	}

	/**
	 * Get films
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFilms()
	{
		return $this->films;
	}


	public function __toString()
	{
		return $this->name;
	}
	
}