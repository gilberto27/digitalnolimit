<?php

namespace Digital\NolimitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Digital\NolimitBundle\Entity\Film;
use Digital\NolimitBundle\Entity\Categorie;

use Digital\NolimitBundle\Repository\FilmRepository;
use Digital\NolimitBundle\Form\FilmType;



class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
         $em = $this->getDoctrine()->getManager();
        $films = $em->getRepository('DigitalNolimitBundle:Film')->listeDesFilms($request->get('page'));
       
    // Controller Action

    # Count of ALL posts (ie: `20` posts)
    $totalPosts = $films->count();//total de films
    
    
    $limit = 2; //nombre de films par page
    $maxPages = ceil($totalPosts / $limit);
    $thisPage = $request->get('page');


        
        
        return $this->render('DigitalNolimitBundle:Default:index.html.twig',
                compact('films', 'maxPages', 'thisPage'));
    }
    
    public function addAction(Request $request)
    { 

        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);
        //$em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
                  $file = $film->getFile()->getFile();
                  if(!empty($file)){
                   $fileName = $this->get('digital_no_limit.file_uploader')->upload($file);
                   
                  $film->getFile()->setUrl( $fileName);
                   $film->getFile()->setAlt($fileName);
                   }
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();
             //dump($request);
            return $this->redirect($this->generateUrl(
                'digital_nolimit_show',
                array('id' => $film->getId())
            ));

        }
        
        return $this->render('DigitalNolimitBundle:Default:add.html.twig', ['form' => $form->createView()]);
    }
     
    public function deleteAction($id)
    { 
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('DigitalNolimitBundle:Film')->find($id);
       
        $em->remove($film);
        $em->flush();
        
        return $this->redirectToRoute('digital_nolimit_homepage');

    }
    
    public function showAction($id)
    {
        $film = $this->getDoctrine()->getRepository('DigitalNolimitBundle:Film')->find(intval($id));

        //dump($book);

        if (!$film) {
            throw $this->createNotFoundException(
                'No film found for id ' . $id
            );
        }
        return $this->render('DigitalNolimitBundle:Default:detail.html.twig', ['film' => $film]);
    }
    
    public function filmsParCategorieAction($id)
    {
        
        
        if (!$id) {
            throw $this->createNotFoundException(
                'No film found for id ' . $id
            );
        }
        $em = $this->getDoctrine()->getManager();
        $films = $em->getRepository('DigitalNolimitBundle:Film')->listeDesFilmsParCategerie($id);
        return $this->render('DigitalNolimitBundle:Default:film-par-categorie.html.twig', ['films' => $films]);
    }
    /**
     * recherche d'un film par titre, categorie,
     * 
     * @param type $id
     * @return type
     * @throws type
     */
    public function rechercheAction()
    {
       $em = $this->getDoctrine()->getManager();
        $films = $em->getRepository('DigitalNolimitBundle:Film')->rechercherFilm("Piège",2);
        
        return $this->render('DigitalNolimitBundle:Default:recherche.html.twig');
    }
}
