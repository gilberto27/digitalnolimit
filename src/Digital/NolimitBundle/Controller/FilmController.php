<?php

namespace Digital\NolimitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Digital\NolimitBundle\Entity\Film;

class FilmController extends Controller
{
    public function indexAction()
    {
        $films = $this->getDoctrine()
           ->getRepository('DigitalNolimitBundle:Film')
           ->findAll();
        
        return $this->render('DigitalNolimitBundle:Film:index.html.twig',$films);
    }
    
    public function addAction()
    { 
        return $this->render('DigitalNolimitBundle:Film:ajout.html.twig');
    }
    
    public function editAction($id)
    { 
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('DigitalNolimitBundle:Film')->find($id);
        
        return $this->render('DigitalNolimitBundle:Film:edit.html.twig');
    }
    
    public function deleteAction($id)
    { 
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('DigitalNolimitBundle:Film')->find($id);
        
        $em->flush();
        $em->remove($film);
        
        return $this->redirectToRoute('home');
    }
}
