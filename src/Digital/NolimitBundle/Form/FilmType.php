<?php

namespace Digital\NolimitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Digital\NolimitBundle\Repository\CategorieRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Image;
use Digital\NolimitBundle\Form\PhotoType;

class FilmType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre')->add('description', TextareaType::class ,['label'=>"description"])
                ->add('file', PhotoType::class)      
                 ->add('categorie', EntityType::class, array(
               'class'         =>  'DigitalNolimitBundle:Categorie',
               'choice_label'  => 'name',
               'multiple'      => false,
               'query_builder' => function(CategorieRepository $repository)  {
                   return $repository->allCategories();
               }
           ))
                    ->add('save',SubmitType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Digital\NolimitBundle\Entity\Film',
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'digital_nolimitbundle_film';
    }


}
