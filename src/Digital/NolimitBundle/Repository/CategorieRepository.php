<?php
/**
 * Created by PhpStorm.
 * User: missamou
 * Date: 28/10/16
 * Time: 21:25
 */

namespace Digital\NolimitBundle\Repository;


class CategorieRepository extends \Doctrine\ORM\EntityRepository
{
    /** renvoie toutes les catégories
     * @return type
     */
    public function allCategories() {
        return $this
                        ->createQueryBuilder('c');
    }

}